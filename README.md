# AI Pancake Game (elastic beanstock)

## Due to ending trial of elastic beanstock instance, this game package was moved to a bitbucket repository.

This repository is contains a zip folder for the AI Pancake game to be uploaded to an AWS (Amazon Web Services) Elastic Beanstock Instance.

This repository is an extension from the original AI Pancake game. Where the original AI code written in C++ was transformed to web Assembly to then be used in a more modern fashion for users to play through
a modern web browser (firefox, chrome, safari).